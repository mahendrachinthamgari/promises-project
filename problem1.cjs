const fs = require('fs');

function invoke(folderName) {
    return createDirectory(folderName)
        .then((folderName) => {
            return createFiles(folderName);
        })
        .then((fileNamesArray) => {
            deleteFiles(fileNamesArray);
        })
        .catch((err) => {
            if (err) {
                reject(err);
            }
        });
}

function createDirectory(folderName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(folderName, { recursive: true }, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('Directory Created Sucessfully');
                resolve(folderName);
            }
        });
    });
}

function createFiles(folderName) {
    return new Promise((resolve, reject) => {
        let randomNumber = Math.round(Math.random() * 10);
        let fileNamesArray = Array(randomNumber).fill(0).map((element, index) => {
            return `${folderName}/file${index}.json`;
        });
        let createdPromises = fileNamesArray.map((fileName, index) => {
            return createEachFile(fileName, index)
        });
        Promise.all(createdPromises)
            .then((values) => {
                console.log(values);
                console.log('All files created Sucessfully')
                resolve(fileNamesArray);
            })
            .catch((err) => {
                reject(err);
            })
    });
}

function createEachFile(filename, index) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filename, 'This file created', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`file${index}.json created sucessfully`);
            }

        });
    });
}


function deleteFiles(fileNamesArray) {
    return new Promise((resolve, reject) => {
        const deletedPromises = fileNamesArray.map((fileName, index) => {
            return deleteEachFile(fileName, index);
        });
        Promise.all(deletedPromises)
            .then((fromresolve) => {
                console.log(fromresolve);
                console.log('All files Deleted Sucessfully');
                resolve();
            })
            .catch((err) => {
                reject(err);
            });
    });
}

function deleteEachFile(fileName, index) {
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`file${index}.json deleted sucessfully`);
            }
        });
    });
}


module.exports = invoke;