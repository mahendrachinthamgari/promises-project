const fs = require('fs');
const path = require('path');


const upperCaseDataFilePath = path.join(__dirname, './upper_case_data.txt');
const sentencesDataFilePath = path.join(__dirname, './sentences_data.txt');
const sortedSentencesPath = path.join(__dirname, './sorted_sentences.txt');
const fileNamesTxtpath = path.join(__dirname, './filenames.txt');


function invoke(lipsumPath) {
    return new Promise((resolve, reject) => {

        readLipsum(lipsumPath)
            .then((data) => {
                return converteDataToUpperCase(data);
            })
            .then((data) => {
                return splitThesentencesLowerCase(data);
            })
            .then((data) => {
                return sortedSentences(data);
            })
            .then((message) => {
                console.log(message);
                return deleteFiles(fileNamesTxtpath);
            })
            .then((message) => {
                console.log(message);
                resolve('All operations sucessful');
            })
            .catch((err) => {
                if (err) {
                    reject(err);
                }
            });

    });
}


function readLipsum(lipsumPath) {
    return new Promise((resolve, reject) => {
        fs.readFile(lipsumPath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                console.log('File read Sucessful');
                resolve(data);
            }
        });
    });
}

function converteDataToUpperCase(data) {
    return new Promise((resolve, reject) => {
        const convertedData = data.toUpperCase();
        fs.writeFile(upperCaseDataFilePath, convertedData.toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('Data Converted and written to new file');
                writtenFileName(upperCaseDataFilePath + '\n')
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((err) => {
                        console.error(err);
                    });
                resolve(data);
            }
        });
    });
}

function writtenFileName(data, fileNamesTxtpath = path.join(__dirname, './filenames.txt')) {
    return new Promise((resolve, reject) => {
        fs.appendFile(fileNamesTxtpath, data.toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('File name added to filenames.txt');
            }
        });
    });
}

function splitThesentencesLowerCase(data) {
    return new Promise((resolve, reject) => {
        const convertedData = data.toLowerCase().split('. ').join('.\n');
        fs.writeFile(sentencesDataFilePath, convertedData.toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('Sentences File created Sucessfully');
                writtenFileName(sentencesDataFilePath + '\n')
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((err) => {
                        console.error(err);
                    });
                resolve(convertedData);
            }
        });
    });
}

function sortedSentences(data) {
    return new Promise((resolve, reject) => {
        const sortedDataSentences = data.split('\n').sort().slice(3);
        fs.writeFile(sortedSentencesPath, sortedDataSentences.join('\n').toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                writtenFileName(sortedSentencesPath)
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((err) => {
                        console.error(err);
                    });
                resolve('Sotred file created');
            }
        });
    });
}

function deleteFiles(fileNamesTxtpath) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileNamesTxtpath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                const fileNamesPromises = data.split('\n').map((filename) => {
                    return deleteEachFile(filename);
                });
                Promise.all(fileNamesPromises)
                    .then((message) => {
                        console.log(message);
                        resolve('All files deleted Sucessfully');
                    })
                    .catch((err) => {
                        console.error(err);
                    });
            }
        });
    });
}

function deleteEachFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`${fileName} deleted sucessfully.`);
            }
        });
    });
}


module.exports = invoke;
